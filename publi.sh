#/bin/bash

if [ "$#" == 0 ]; then
  echo "Il faut mettre les fichiers à publier en argument"
  exit -1
fi

BRANCH_NAME=$(date -Isecond)
BRANCH_NAME=${BRANCH_NAME/:/}
BRANCH_NAME=${BRANCH_NAME/:/}
BRANCH_NAME=${BRANCH_NAME/:/}
BRANCH_NAME=${BRANCH_NAME/+/}
git switch site
git checkout -b presite/$BRANCH_NAME
git checkout main -- $@

echo -n "Voir ce qui va être modifié sur la branche site [Y/n]"
read -r site
if [ "x$site" == "x" ] ; then
git diff site presite/$BRANCH_NAME
fi
if [ "x$site" == "xY" ] ; then
git diff site presite/$BRANCH_NAME
fi

echo -n "Envoyer la branche"
read -r sent
if [ "x$send" == "x" ] ; then
git push origin presite/$BRANCH_NAME
fi
if [ "x$send" == "xY" ] ; then
git push origin presite/$BRANCH_NAME
fi

git switch main
git branch -d presite/$BRANCH_NAME
