# Ensemble de petits cours d'informatique

Page créée : https://e-nsi.forge.aeif.fr/cours

- Un ensemble de petits cours indépendants **qui tiennent sur une seule page**.
- Ils seront rangés dans quelques catégories, peu nombreuses.
- Quand un lot de petits cours homogènes constituent un ensemble, ils migrent vers un projet indépendant.

Chaque enseignant peut proposer un petit cours ici, il sera retravaillé en groupe.

Il suffit de proposer un dossier qui contient

- index.md ; un seul fichier markdown
- du matériel annexe…

On en discute sur le forum, section e-nsi/cours… et il intègre le projet.
