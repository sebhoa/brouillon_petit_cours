# Programmation Orientée Objet

## Introduction

Considérons une organisation complexe comme un tournoi de rugby. Ce _système complexe_ est constitué d'un certain nombre d'entités : le tournoi lui-même, caractérisé par divers éléments (dates, lieux, joueurs, etc.). Ces éléments constitutifs sont eux-même des entités plus ou moins complexes. Par exemple on trouve : des équipes, elles-mêmes constituées de groupes de joueurs (titulaires remplaçants) etc. 

La réalisation de ce tournoi se concrétise par des interactions entre les différentes entités du système complexe.

En Informatique, la **Programmation Orienté Objet** (POO) est un paradigme de programmation datant des années 60, qui s'efforce de décrire un problème comme un ensemble d'entités (on parle plutôt d'**objets**) qui interagissent. Créer ces objets et leurs interactions sera alors le moyen de résoudre le problème. De nos jours, de nombreux langages de programmation permettent de programmer avec des objets. Dans la suite nous allons voir le vocabulaire associé à ce paradigme et comment il peut être mis en œuvre en Python. Nous prendrons un exemple bien plus simple que la gestion d'un tournoi de rugby.

## Une version minimaliste de D&D

### Le contexte réel

Donjons et Dragons est un jeu de rôle sur table de genre médiéval-fantastique. Le jeu a été créé dans les années 1970 par les Américains Gary Gygax et Dave Arneson. De façon très schématisée, le but du jeu consiste à réaliser diverses quêtes à l'aide d'un groupe de personnages aventuriers.

Dans notre D&D minimaliste, Nous allons nous intéresser aux personnages caractérisés chacun par :

- un nom
- une spécialité (magicien, druide, guerrier)
- des points de vie

Un personnage peut attaquer un autre personnage et lui infliger des points de dégâts directement retirés de ses points de vie. Il peut également soigner une cible (un autre personnage, par un sort de soin, une potion ou autre) et remonter le niveau de vie la cible.

Par exemple, Arthur est un magicien possédant $80$ points de vie. Dramj une guerrière de $120$ points de vie. Arthur et Dramj font équipe et pourchassent le terrible Orz, un guerrier de $150$ points de vie.

### Le vocabulaire

Dans la modélisation POO de notre mini D&D, l'entité _personnage_ de D&D est un objet informatique `Personnage`. _Arthur_, _Dramj_ et _Orz_ sont des **instances** de `Personnage`. 

Toute instance de `Personnage` possède 5 **attributs** (pour l'instant) :

- 3 **propriétés** ou **attributs données** qui sont les caractéristiques d'un personnage : `nom`, `specialite` et `point_de_vie`
- 2 **méthodes** qui sont les actions réalisables par un personnage, ici `attaque` et `soigne`

Nous allons voir comment mettre en place ces éléments en Python.

## Les objets en Python

En réalité, nous avons déjà côtoyé des objets puisque tous les types de Python sont des objets. Par exemple, lorsque nous écrivons :

???+ example "Un exemple d'objet connu"

    ```python
    ma_liste = ['pomme', 'banane']
    ma_liste.append('poire')
    ```

    Nous faisons de la programmation objet. `ma_liste` est un objet de type `list` qui réalise une action de liste : _s'ajouter_ un élément à la fin de son contenu.

Les chaine de caractères sont aussi des objets :

???+ example "Un autre exemple avec `str`"

    ```python
    >>> jour = 'lundi'
    >>> jour.upper()
    'LUNDI'
    ```

L'écriture `ma_liste.append(...)` et `jour.upper()` est appelée **notation pointée**, caractéristique de la programmation orientée objet.

## Créer ses propres objets

Python utilise un système de **classe** pour créer des objets. Ainsi, nous pouvons créer notre objet `Personnage` comme ceci :

!!! tip "Notre première `class`"

    ```python
    class Personnage:
        pass
    ```

Le mot-clé `class` (**attention** pas de `e` à la fin : il s'agit du mot anglais) suivi du nom de notre objet, ici `Personnage`. 

???+ tip "Bonne pratique de nommage"

    En Python, les noms de classe suivent la règle `CamelCase` pour le nommage des classes. Cette convention suit la règle simple : chaque mot qui constitue le nom commence par une majuscule, suivi de minuscules, pas de _souligné_ ou _tiret bas_ comme dans le nommage des variables classiques et fonctions.

Dès lors, la création d'une instance se fait en appelant le **constructeur** de la classe :

???+ example "Notre première instance"

    ```python
    arthur = Personnage()
    ```

    La variable `arthur` référence une instance de `Personnage`. Mais pour l'instant notre _arthur_ ne possède ni nom, ni spécialité, ni points de vie. Nous pouvons lui ajouter :

    ```python
    arthur.nom = 'Arthur'
    arthur.specialite = 'magicien'
    arthur.point_de_vie = 80
    ```

Il faudrait recommencer ces instructions pour nos deux autres personnages. Avouons que cela n'est guère pratique. Heureusement, lors de la construction de la classe, nous pouvons, grâce à une méthode spéciale appelée l'**initialiseur** mettre en place les attributs de données de nos instances.

!!! important "La méthode `__init__`"

    ```python linenums="1"
    class Personnage:

        def __init__(self, nom, specialite, points_de_vie):
            self.nom = nom
            self.specialite = specialite
            self.points_de_vie = points_de_vie
    ```

### Quelques explications

L'initialiseur, est une méthode particulière nommée un peu bizarrement : `__init__`. Elle se définit comme une simple fonction : sa ligne d'entête commence par `def` et elle possède des paramètres. Comme **toutes les méthodes** d'une classe, le premier paramètre fait référence à l'objet lui-même et son nom, par convention, est `self`.

Dès que nous voudrons faire appel à un attribut de notre objet, nous devrons le préfixer par ce `self`. Ainsi dans l'écriture :

```python
self.nom = nom
```

La propriété de notre instance en création est `self.nom`, à ne pas confondre avec la variable `nom` qui, au moment de la création va référencer le nom choisi (par exemple la chaine de caractères `'Arthur'`).

???+ warning "À noter..."

    - Le nom assez particulier de l'initialiseur : `__init__` qui commence et finit par deux tirets bas ; toutes les méthodes spéciales adopterons ce genre de nom ;
    - Dans la définition ci-dessus, notez que la définition de l'initialiseur commence par le mot clé `def` comme pour la définition d'une simple fonction ;
    - Les définitions relatives à la classe sont indentées pour _entrer_ dans le corps de la `class`
    - Le premier paramètre de la méthode `__init__` est `self` ; il s'agit d'une convention : ce paramètre fait référence à l'instance en création 

La création de nos personnages est alors plus commode :

???+ example "Trois instances"

    ```python
    arthur = Personnage('Arthur', 'magicien', 80)
    dramj = Personnage('Dramj', 'guerriere', 120)
    orz = Personnage('Orz', 'guerrier', 150)
    ```

    Et nous pouvons vérifier que chaque personnage à son propre nom, sa propre spécialité, ses propres points de vie :

    ```pycon
    >>> arthur.nom
    'Arthur'
    >>> dramj.nom
    'Dramj'
    >>> orz.points_de_vie
    150
    >>> arthur.points_de_vie
    80
    ```

### D'autres méthodes

Il nous faut ajouter les méthodes `attaque` et `soigne` et pour que ces méthodes, qui vont modifier la valeur de `points_de_vie`, nous allons introduire une $4^e$  propriété pour stocker le nombre maximum de points de vie.

!!! important "Complétion de la class `Personnage`"

    ```python linenums="1"
    class Personnage:

        def __init__(self, nom, specialite, points_de_vie):
            self.nom = nom
            self.specialite = specialite
            self.points_de_vie = points_de_vie
            self.pv_max = points_de_vie
    
        def attaque(self, cible, degats):
            cible.points_de_vie -= degats

        def soigne(self, cible, qte_soin):
            cible.points_de_vie += qte_soin
    ```

Voyons quelques mises en œuvre :

???+ example "Les objets en action..."

    Arthur attaque Orz et lui inflige $12$ points de dégâts ; ce dernier réplique et blesse Dramj pour 18 points de dégâts :

    ```pycon
    >>> arthur.attaque(orz, 12)
    >>> orz.attaque(dramj, 15)
    ```

    Dramj prend une potion de soin qui le soigne de 10 points de vie :

    ```pycon
    >>> dramj.soigne(dramj, 10)
    ```

???+ attention "À propos de copie"

    Attention à ne pas croire que les instructions suivantes réalisent une copie d'objet :

    ```python
    >>> un_mage = Personnage('Magus', 'magicien', 100)
    >>> jumeau = un_mage
    ```

    `un_mage` et `jumeau` référencent la même instance de `Personnage`. 
    
    ![un seul](un_seul_perso.svg){ width="200" }
    
    Et bien sûr, modifier l'objet par l'une ou l'autre des variables est possible :

    ```python
    >>> jumeau.points_de_vie -= 10
    >>> un_mage.points_de_vie
    90
    ```

    Si pour le problème à résoudre la création d'une copie est nécessaire, alors il faut programmer la méthode adéquate, par exemple :

    ```python
    class Personnage:
        ...

        def copie(self):
            return Personnage(self.nom, self.specialite, self.pv_max) 
    ```

    Nos deux instructions deviennent alors :

    ```python
    >>> un_mage = Personnage('Magus', 'magicien', 100)
    >>> jumeau = un_mage.copie()
    ```

    Les deux variables référencent maintenant deux instances distinctes :

    ![deux persos](deux_persos.svg){ width="400" }


    ```python
    >>> jumeau.points_de_vie -= 10
    >>> jumeau.points_de_vie
    90
    >>> un_mage.points_de_vie
    100
    ```  


## Bonnes pratiques : encapsulation

Dans l'exemple précédent, nous constatons qu'un personnage modifie **directement** une propriété (en l'occurrence les `points_de_vie`) d'un autre objet. En programmation objet cela représente une très mauvaise pratique : les opérations effectuées sont susceptibles de portée atteinte à l'intégrité des informations.

Ainsi, ici, nos personnages pourraient se retrouver avec des points de vie négatifs ou alors un nombre de points de vie exagérément élevés :

???+ example "Modifications interdites..."

    ```pycon
    >>> orz.attaque(dramj, 200)
    >>> dramj.points_de_vie
    -85  # 120 - 15 + 10 - 200
    >>> arthur.soigne(dramj, 220)
    >>> dramj.points_de_vie
    135
    ```

Le principe d'**encapsulation[^1]** consiste, pour un objet, à ne pas tout exposer : les propriétés et certaines méthodes ne sont pas accessibles de l'extérieur de l'objet. L'objet met alors à disposition de l'extérieur des méthodes pour accéder à ces propriétés (et notamment pour les modifier).

Ces méthodes sont appelées _assesseur_ (pour accéder à la valeur) et _mutateur_ (pour modifier la valeur). Ainsi, pour chaque attribut de données sensible `attr` il faudrait définir une méthode `get_attr` qui permette d'obtenir la valeur de la propriété et une méthode `set_attr` qui permette de changer la valeur associée à la propriété :

???+ tip "L'encapsulation"

    ```python
    class MonObjet:

        def __init__(self, val_attribut):
            self.attr = valeur_attribut
        
        def get_attr(self):
            return self.attr

        def set_attr(self, nouvelle_valeur):
            self.attr = nouvelle_valeur
    ```

Si elle respecte les bonnes pratiques, cette façon de faire alourdit énormément le code et si nous reprenons notre exemple des personnages de D&D, nous n'avons pas besoin d'une méthode `set_nom` puisqu'a priori le nom d'un personnage une fois choisi n'est pas amené à changer et nous n'avons pas envie de remplacer le très pratique `arthur.nom` par `arthur.get_nom()`.

Par contre, une méthode pour la modification des `points_de_vie` qui peut contrôler la validité de la nouvelle valeur (qui doit rester entre $0$ et le nombre maximum de points de vie du personnage).

En adoptant ce compromis, voici notre nouvelle classe `Personnage`:


!!! important "Encapsulation avec la class `Personnage`"

    ```python linenums="1"
    class Personnage:

        def __init__(self, nom, specialite, points_de_vie):
            self.nom = nom
            self.specialite = specialite
            self.points_de_vie = points_de_vie
            self.pv_max = points_de_vie
    
        def set_points_de_vie(self, pts_vie):
            if pts_vie < 0:
                self.points_de_vie = 0
            elif pts_vie > self.pv_max:
                self.points_de_vie = self.pv_max
            else:
                self.points_de_vie = pts_vie

        def attaque(self, cible, degats):
            cible.set_points_de_vie(cible.points_de_vie - degats)

        def soigne(self, cible, qte_soin):
            cible.set_points_de_vie(cible.points_de_vie + qte_soin)
    ```


## Exercices

###  Quiz

On considère la classe `Monstre` suivante qui modélise une entité monstre caractérisée par un nom, un nombre de points de vie et des dégâts (lors d'une attaque du monstre) :

```python linenums="1"
class Monstre:

    def __init__(self, nom, points_de_vie, degats):
        self.nom = nom
        self.points_de_vie = points_de_vie
        self.pv_max = points_de_vie
        self.degats = degats
```

???+ question "Les questions"

    1. `self.nom` est...   

        === "Réponses"

            - [ ] une méthode
            - [ ] un attribut de donnée
            - [ ] une propriété
            - [ ] une méthode spéciale

        === "Solution"

            - [ ] ~~une méthode~~
            - [x] un attribut de donnée
            - [x] une propriété
            - [ ] ~~une méthode spéciale~~

    2. `__init__` est...    

        === "Réponses"

            - [ ] une méthode
            - [ ] un attribut
            - [ ] une propriété
            - [ ] un constructeur

        === "Solution"

            - [x] une méthode
            - [x] un attribut
            - [ ] ~~une propriété~~
            - [ ] ~~un constructeur~~ 

    3. Les méthodes sont...    

        === "Réponses"

            - [ ] des fonctions propres à l'objet
            - [ ] des fonctions sans paramètres
            - [ ] des fonctions dont le premier paramètre est toujours l'objet lui-même
            - [ ] des fonctions comme les autres 

        === "Solution"

            - [x] des fonctions propres à l'objet
            - [ ] ~~des fonctions sans paramètres~~
            - [x] des fonctions dont le premier paramètre est toujours l'objet lui-même
            - [ ] ~~des fonctions comme les autres~~ 


### Un peu de programmation

!!! exercice "Chien"

    On manipule une classe `Chien`...

    [Exercice POO Chien](https://e-nsi.forge.aeif.fr/pratique/N2/700-poo_chien/sujet/){ .md-button }

!!! exercice "Train"

    On manipule une classe `Train`...

    [Exercice POO Chien](https://e-nsi.forge.aeif.fr/pratique/N2/700-poo_train/sujet/){ .md-button }

!!! exercices "D'après des sujets d'épreuves écrites"

    Plusieurs exercices inspirés d'exercices de sujets des épreuves écrites où la programmation orientée objet est abordée.

    [POO dans les épreuves écrites](https://e-nsi.forge.aeif.fr/ecrit/tags/#prog-poo){ .md-button }



[^1]: Python n'est pas très strict sur le mécanisme d'encapsulation, bien moins que d'autres langages orienté objet comme Java ou C++ ; néanmoins des solutions existent qui sont hors programme NSI.
