# Ensemble de petits cours d'informatique

- Un ensemble de petits cours indépendants **qui tiennent sur une seule page**.
- Ils sont rangés dans quelques catégories, peu nombreuses. Sans distinguer première/terminale.
- Quand un lot de petits cours homogènes constituent un ensemble, ils migrent vers un projet indépendant.

